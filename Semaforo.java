package com.company;

public class Semaforo {


    public static String colore = "";
    public static String segno = "";


    //uso il costruttore per inizializzare tutto
    public Semaforo(String colore, String segno) {
        this.colore=colore;
        this.segno =segno;
    }

    //controllo del semaforo
    public static void modifica() {
        switch (colore) {
            case "rosso":
                colore = "verde";
                break;
            case "arancione":
                colore = "rosso";
                break;
            case "verde":
                colore = "arancione";
                break;
        }
    }


    //uso il to string per stampare il tutto
    @Override
    public String toString() {
        return "sono di colore  "+colore+" ed ho il segno verso "+ segno;
    }



}
